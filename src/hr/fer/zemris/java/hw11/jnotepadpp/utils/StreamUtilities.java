package hr.fer.zemris.java.hw11.jnotepadpp.utils;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

/**
 * Utility class for usual static stream methods.
 * 
 * @author Josip Kasap
 *
 */
public class StreamUtilities {
	
	/**
	 * Private constructor to unable creation of this class.
	 */
	private StreamUtilities() {
		super();
	}

	/**
	 * Reads all bytes from given input stream.
	 * @param is input stream
	 * @return all bytes from input stream
	 * @throws IllegalArgumentException if input stream is null value
	 */
	public static byte[] readAllBytes(InputStream is) {
		if (is == null) {
			throw new IllegalArgumentException("Input stream cannot be null value");
		}
		byte[] result = null;
		byte[] buffer = new byte[4*1024];
		int r;
		try {
			while ((r = is.read(buffer)) > 0) {
				if (r == 0) break;
				result = append(result, buffer, r);
			}
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(-1);
		}
		
		return result;
	}

	/**
	 * Helping method that returns buffer that contains all bytes from first buffer and 
	 * all bytes of second buffer.
	 * @param result first buffer
	 * @param buffer second buffer
	 * @param bufferLen length of second buffer
	 * @return buffer that contains all bytes from first buffer and all bytes of second buffer
	 */
	private static byte[] append(byte[] result, byte[] buffer, int bufferLen) {
		if (buffer == null || bufferLen == 0) {
			return result;
		}
		byte[] newResult;
		if (result == null) {
			newResult = new byte[bufferLen];
			for (int i = 0; i < bufferLen; i++) {
				newResult[i] = buffer[i];
			}
			return newResult;
		} else {
			newResult = new byte[result.length + bufferLen];
		}
		
		for (int i = 0; i < result.length; i++) {
			newResult[i] = result[i];
		}
		for (int i = 0; i < bufferLen; i++) {
			newResult[i + result.length] = buffer[i];
		}
		return newResult;
	}
	
	/**
	 * Writes whole text into given file using UTF-8 charset. If writing fails for
	 * any reason this method will do nothing, it will not throw any exception.
	 * @param fileName output file
	 * @param newFileText text that is to be written on output file
	 * @throws IllegalArgumentException if any argument is null value
	 */
	public static void writeOnFile(File fileName, String newFileText) {
		if (fileName == null || newFileText == null) {
			throw new IllegalArgumentException("This method does not allow null values of arugments.");
		}
		try(BufferedOutputStream writer = new BufferedOutputStream(new FileOutputStream(fileName))) {
			writer.write(newFileText.getBytes(StandardCharsets.UTF_8));
			writer.flush();
		} catch (IOException ignorable) {}
	}
}
