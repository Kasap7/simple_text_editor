package hr.fer.zemris.java.hw11.jnotepadpp.local;

import java.util.ArrayList;
import java.util.List;

/**
 * AbstractLocalizationProvider is simple implementation of {@link ILocalizationProvider}
 * which stores listeners in {@link ArrayList}, and provides implementation of all methods
 * except {@link #getString(String)}. This class also provides protected method {@link #fire()}
 * which notifies all listeners, this method should be after language has changed.
 * 
 * @author Josip Kasap
 *
 */
public abstract class AbstractLocalizationProvider implements ILocalizationProvider {
	
	/** List of listeners. */
	protected List<ILocalizationListener> listeners;
	
	/**
	 * Constructs new AbstractLocalizationProvider.
	 */
	public AbstractLocalizationProvider() {
		listeners = new ArrayList<>();
	}
	
	@Override
	public void addLocalizationListener(ILocalizationListener listener) {
		if (listener == null) return;
		listeners.add(listener);
	}
	
	@Override
	public void removeLocalizationListener(ILocalizationListener listener) {
		if (listener == null) return;
		listeners.remove(listener);
	}
	
	/**
	 * Notifies all listeners, calls {@link #localizationChanged()} on all registered listener.
	 */
	protected void fire() {
		for (ILocalizationListener listener : listeners.toArray(new ILocalizationListener[listeners.size()])) {
			listener.localizationChanged();
		}
	}
}
