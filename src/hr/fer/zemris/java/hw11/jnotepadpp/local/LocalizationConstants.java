package hr.fer.zemris.java.hw11.jnotepadpp.local;
import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;

/**
 * LocalizationConstants is utility class which contains values of String constants used 
 * for internationalization of {@link JNotepadPP}. This class contains all key for localization
 * provider which is used by {@link JNotepadPP} for internationalization.
 * 
 * @author Josip Kasap
 *
 */
public class LocalizationConstants {
	
	/*
	 * All of these constants are self-explanatory, there is no reason to write
	 * javadoc for all of them.
	 * Javadoc of each of these constants would be like:
	 * 
	 * String value for <variable_name> key of localization.
	 */
	
	public static final String LN_KEY = "ln";
	public static final String LENGTH_KEY = "length";
	public static final String COL_KEY = "col";
	public static final String SEL_KEY = "sel";
	public static final String FILE_KEY = "file";
	public static final String EDIT_KEY = "edit";
	public static final String TOOLS_KEY = "tools";
	public static final String STATS_KEY = "statistics";
	public static final String LANG_KEY = "languages";
	public static final String NEW_KEY = "new";
	public static final String NEW_DESC_KEY = "newDesc";
	public static final String OPEN_KEY = "open";
	public static final String OPEN_DESC_KEY = "openDesc";
	public static final String SAVE_KEY = "save";
	public static final String SAVE_DESC_KEY = "saveDesc";
	public static final String SAVE_AS_KEY = "saveAs";
	public static final String SAVE_AS_DESC_KEY = "saveAsDesc";
	public static final String CLOSE_KEY = "close";
	public static final String CLOSE_DESC_KEY = "closeDesc";
	public static final String EXIT_KEY = "exit";
	public static final String EXIT_DESC_KEY = "exitDesc";
	public static final String COPY_KEY = "copy";
	public static final String COPY_DESC_KEY = "copyDesc";
	public static final String CUT_KEY = "cut";
	public static final String CUT_DESC_KEY = "cutDesc";
	public static final String PASTE_KEY = "paste";
	public static final String PASTE_DESC_KEY = "pasteDesc";
	public static final String INFO_KEY = "info";
	public static final String INFO_DESC_KEY = "infoDesc";
	public static final String CASE_KEY = "case";
	public static final String UPPERCASE_KEY = "uppercase";
	public static final String UPPERCASE_DESC_KEY = "uppercaseDesc";	
	public static final String LOWERCASE_KEY = "lowercase";
	public static final String LOWERCASE_DESC_KEY = "lowercaseDesc";
	public static final String INVERTCASE_KEY = "invertcase";
	public static final String INVERTCASE_DESC_KEY = "invertcaseDesc";
	public static final String SORT_KEY = "sort";
	public static final String ASCENDING_KEY = "ascending";
	public static final String ASCENDING_DESC_KEY = "ascendingDesc";
	public static final String DESCENDING_KEY = "descending";
	public static final String DESCENDING_DESC_KEY = "descendingDesc";
	public static final String UNIQUE_KEY = "unique";
	public static final String UNIQUE_DESC_KEY = "uniqueDesc";
	public static final String SYS_MSG_KEY = "sysMsg";
	public static final String EXIT_MSG_KEY = "exitMsg";
	public static final String SAVE_FILE_KEY = "saveFile";
	public static final String OPEN_FILE_KEY = "openFile";
	public static final String NOT_EXIST_KEY = "notExist";
	public static final String ERROR_KEY = "error";
	public static final String FILE_ERROR_KEY = "fileError";
	public static final String ALREADY_EXISTS_KEY = "alreadyExists";
	public static final String CANT_CREATE_KEY = "cantCreate";
	public static final String OVERWRITE_KEY = "overwrite";
	public static final String WARNING_KEY = "warning";
	public static final String CHARS_KEY = "chars";
	public static final String NON_BLANK_KEY = "nonBlank";
	public static final String LINES_KEY = "lines";
	public static final String STATS_INFO_KEY = "statsInfo";
	public static final String NEW_FILE_KEY = "newFile";
	
	/** String value of localization String for Croatian language. */
	public static final String HR = "hr";
	
	/** String value of localization String for German language. */
	public static final String EN = "en";
	
	/** String value of localization String for English language. */
	public static final String DE = "de";

	/**
	 * Private constructor only to unable creation of this class.
	 */
	private LocalizationConstants() {
		super();
	}
}
