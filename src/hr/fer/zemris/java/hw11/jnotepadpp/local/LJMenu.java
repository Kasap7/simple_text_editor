package hr.fer.zemris.java.hw11.jnotepadpp.local;
import javax.swing.JMenu;

/**
 * Localizable implementation of {@link JMenu} which can change its content depending
 * of current language of given localization provider. Only difference between this menu, 
 * and {@link JMenu} is that each time language of provider changes, this menu will change 
 * its text to value given by key of that provider.
 * 
 * @author Josip Kasap
 *
 */
public class LJMenu extends JMenu {
	/** Default serial version. */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Constructs new LJMenu with given key value, and localization provider that translates that
	 * key value.
	 * @param key key value of localization provider
	 * @param lp localization provider
	 * @throws IllegalArgumentException if any argument is null value
	 */
	public LJMenu(String key, ILocalizationProvider lp) {
		super();
		if (key == null || lp == null) {
			throw new IllegalArgumentException("Null values are not valid arguments for this constructor.");
		}
		setText(lp.getString(key));
		lp.addLocalizationListener(() -> {
			setText(lp.getString(key));
		});
		
	}

}
