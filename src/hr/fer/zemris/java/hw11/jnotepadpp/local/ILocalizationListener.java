package hr.fer.zemris.java.hw11.jnotepadpp.local;

/**
 * ILocalizationListener describes some listener for changes of localization language.
 * When localization language changes, all listeners will be noticed via 
 * {@link #localizationChanged()} method.
 * 
 * @author Josip Kasap
 *
 */
public interface ILocalizationListener {
	
	/**
	 * Method that describes what is listener supposed to do after localization
	 * language has changed.
	 */
	void localizationChanged();
}
