package hr.fer.zemris.java.hw11.jnotepadpp.local;

import javax.swing.JLabel;

/**
 * Localizable implementation of {@link JLabel} which can change its content depending
 * of current language of {@link ILocalizationProvider}. This label has been modified
 * in different format that usual JLabel. This label expects provider, and key for current
 * value of text, and addition text value. <p> Each time new text has been set to this label,
 * label will add that text to current value given key. In another words if you set text 
 * of this label to abc, and lets assume that current value of key for given provider is
 * ABC, text of label will be: ABCabc.
 * 
 * @author Josip Kasap
 *
 */
public class LJLabel extends JLabel {
	/** Default serial version. */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructs new LJLabel with given key for localization, current text value, horizontal alignment
	 * and localization provider.
	 * @param key key for localization
	 * @param text text value
	 * @param horizontalAlignment horizontal alignment
	 * @param lp localization provider
	 * @throws IllegalArgumentException if any argument is null value
	 */
	public LJLabel(String key, String text, int horizontalAlignment, ILocalizationProvider lp) {
		super();
		if (key == null || lp == null || text == null) {
			throw new IllegalArgumentException("Null values are not valid arguments for this constructor.");
		}
		this.setHorizontalAlignment(horizontalAlignment);
		setName(lp.getString(key));
		setText(text);
		lp.addLocalizationListener(() -> {
			setName(lp.getString(key));
			setText(text);
		});
	}
	
	@Override
	public void setText(String text) {
		super.setText(getName() + text);
	}
}
