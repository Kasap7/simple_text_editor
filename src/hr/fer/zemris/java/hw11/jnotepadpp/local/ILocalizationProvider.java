package hr.fer.zemris.java.hw11.jnotepadpp.local;

/**
 * ILocalizationProvider describes some class which can provide translation for
 * some string key. Implementations of ILocalizationProvider provide translations 
 * for multiple languages, and they can change its language dynamically.
 * 
 * @author Josip Kasap
 *
 */
public interface ILocalizationProvider {
	
	/**
	 * Adds new listener for language changes.
	 * @param listener listener for language changes
	 */
	void addLocalizationListener(ILocalizationListener listener);
	
	/**
	 * Removes language change listener
	 * @param listener listener that is no longer going to be listener for
	 * 			language changes of this provider
	 */
	void removeLocalizationListener(ILocalizationListener listener);
	
	/**
	 * Returns String value for current key. String value is determined upon current
	 * language of this provider.
	 * @param key key for String value
	 * @return String value of given key
	 */
	String getString(String key);
}
