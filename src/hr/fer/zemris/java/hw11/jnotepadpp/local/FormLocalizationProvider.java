package hr.fer.zemris.java.hw11.jnotepadpp.local;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;

/**
 * FormLocalizationProvider is implementation of {@link ILocalizationProvider} which binds
 * some JFrame, connecting it to provider while frame is opened, and disconnecting when frame
 * is closed.
 * 
 * @author Josip Kasap
 *
 */
public class FormLocalizationProvider extends LocalizationProviderBridge implements WindowListener {

	/**
	 * Constructs new FormLocalizationProvider with given provider, and frame.
	 * @param provider provider that does the translating
	 * @param frame frame that is to be bind with this provider
	 * @throws IllegalArgumentException if any argument is null value
	 */
	public FormLocalizationProvider(ILocalizationProvider provider, JFrame frame) {
		super(provider);
		if (frame == null) {
			throw new IllegalArgumentException("Frame cannot be null value.");
		}
		frame.addWindowListener(this);
	}

	@Override
	public void windowActivated(WindowEvent arg0) {}
	@Override
	public void windowClosing(WindowEvent arg0) {}
	@Override
	public void windowDeactivated(WindowEvent arg0) {}
	@Override
	public void windowDeiconified(WindowEvent arg0) {}
	@Override
	public void windowIconified(WindowEvent arg0) {}
	
	@Override
	public void windowOpened(WindowEvent arg0) {
		connect();
	}
	
	@Override
	public void windowClosed(WindowEvent arg0) {
		disconnect();
	}
}
