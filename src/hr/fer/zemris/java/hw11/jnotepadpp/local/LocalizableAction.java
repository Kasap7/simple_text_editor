package hr.fer.zemris.java.hw11.jnotepadpp.local;

import javax.swing.AbstractAction;
import javax.swing.Action;

/**
 * LocalizableAction is implementation of {@link AbstractAction} which dynamically changes
 * its name and short description based on current value of localization provider.
 * 
 * @author Josip Kasap
 *
 */
public abstract class LocalizableAction extends AbstractAction {
	
	/** Default serial version. */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Constructs new LocalizableAction with given key for name and description, and with given
	 * localization provider.
	 * @param keyName key for name of localization provider
	 * @param keyDesc key for short description of localization provider
	 * @param lp localization provider
	 */
	public LocalizableAction(String keyName, String keyDesc, ILocalizationProvider lp) {
		super();
		if (keyName == null || lp == null || keyDesc == null) {
			throw new IllegalArgumentException("Null values are not valid arguments for this constructor.");
		}
		putValue(Action.NAME, lp.getString(keyName));
		putValue(Action.SHORT_DESCRIPTION, lp.getString(keyDesc));
		lp.addLocalizationListener(() -> {
			putValue(Action.NAME, lp.getString(keyName));
			putValue(Action.SHORT_DESCRIPTION, lp.getString(keyDesc));
		});
	}
}
