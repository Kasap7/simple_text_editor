package hr.fer.zemris.java.hw11.jnotepadpp.local;

/**
 * LocalizationProviderBridge is decorative bridge provider which holds reference to
 * provider that makes all the work. This class can connect or disconnect himself from
 * that provider, thus creating a bridge with real provider.
 * 
 * @author Josip Kasap
 *
 */
public class LocalizationProviderBridge extends AbstractLocalizationProvider {
	
	/** Flag that determines if this class should be connected to provider. */
	private boolean connected;
	
	/** Provider which does real work. */
	private ILocalizationProvider provider;
	
	/** Listener that can be connected or disconnected with real provider. */
	private ILocalizationListener listener = new ILocalizationListener() {
		@Override
		public void localizationChanged() {
			fire();
		}
	};
	
	/**
	 * Constructs new LocalizationProviderBridge with given provider.
	 * @param provider ILocalizationProvider provider which does real work
	 * @throws IllegalArgumentException if provider is null value
	 */
	public LocalizationProviderBridge(ILocalizationProvider provider) {
		if (provider == null) {
			throw new IllegalArgumentException("ILocalizationProvider cannot be null value.");
		}
		this.provider = provider;
	}
	
	/**
	 * Disconnects itself from provider.
	 */
	public void disconnect() {
		if (connected) {
			provider.removeLocalizationListener(listener);
		}
		connected = false;
	}
	
	/**
	 * Connects itself to provider.
	 */
	public void connect() {
		if (connected) return;
		connected = true;
		provider.addLocalizationListener(listener);
	}

	@Override
	public String getString(String key) {
		if (key == null) {
			throw new IllegalArgumentException("Key cannot be null value.");
		}
		return provider.getString(key);
	}

}
