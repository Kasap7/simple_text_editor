package hr.fer.zemris.java.hw11.jnotepadpp.local;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * LocalizationProvider is implementation of {@link ILocalizationProvider} which
 * implements singleton pattern, allowing users to obtain only one instance of this
 * class, because only one instance of this class exists. With singleton pattern this
 * class can deliver same changes of language to any observer observed to this class.
 * 
 * @author Josip Kasap
 *
 */
public class LocalizationProvider extends AbstractLocalizationProvider {
	
	/** Current language value of this provider, initial value is for English language. */
	private String language;
	
	/** ResourceBundle for translation mapping key values to translations. */
	private ResourceBundle bundle;
	
	/** Only instance of this class. */
	private static final LocalizationProvider INSTANCE = new LocalizationProvider();
	
	/**
	 * Private constructor for singleton pattern.
	 */
	private LocalizationProvider() {
		super();
		language = LocalizationConstants.EN;
		Locale locale = Locale.forLanguageTag(language);
		bundle = ResourceBundle.getBundle("hr.fer.zemris.java.hw11.jnotepadpp.local.translations", locale);
	}
	
	@Override
	public String getString(String key) {
		if (key == null) {
			throw new IllegalArgumentException("Key cannot be null value.");
		}
		return bundle.getString(key);
	}
	
	/**
	 * Returns unique instance of this class.
	 * @return unique instance of this class
	 */
	public static LocalizationProvider getInstance() {
		return INSTANCE;
	}
	
	/**
	 * Setter for language property.
	 * @param language new language
	 * @throws IllegalArgumentException if language is null value
	 */
	public void setLanguage(String language) {
		if (language == null) {
			throw new IllegalArgumentException("Language cannot be null value.");
		}
		this.language = language;
		Locale locale = Locale.forLanguageTag(language);
		bundle = ResourceBundle.getBundle("hr.fer.zemris.java.hw11.jnotepadpp.local.translations", locale);
		fire();
	}
	
	/**
	 * Getter for language property.
	 * @return current language
	 */
	public String getLanguage() {
		return language;
	}
}
