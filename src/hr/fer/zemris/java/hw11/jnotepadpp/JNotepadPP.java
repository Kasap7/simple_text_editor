package hr.fer.zemris.java.hw11.jnotepadpp;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.Collator;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Function;

import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

import hr.fer.zemris.java.hw11.jnotepadpp.local.FormLocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.local.LJLabel;
import hr.fer.zemris.java.hw11.jnotepadpp.local.LJMenu;
import hr.fer.zemris.java.hw11.jnotepadpp.local.LocalizableAction;
import hr.fer.zemris.java.hw11.jnotepadpp.local.LocalizationConstants;
import hr.fer.zemris.java.hw11.jnotepadpp.local.LocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.utils.StreamUtilities;

/**
 * JNotepad++ is textual editor which can edit multiple files at once. This editor
 * provides usual function for editors such as copy, cut, paste for editing, file options
 * for opening, closing and save file. This editor is internationalized, and supports
 * 3 different languages, initial language is English.
 * 
 * @author Josip Kasap
 *
 */
public class JNotepadPP extends JFrame {
	
	/** Serial number. */
	private static final long serialVersionUID = 42L;
	
	/** Icon for saved and unmodified file. */
	private static final ImageIcon greenDisk = IconLoader.getImageIcon("icons/greenDisk.png");
	
	/** Icon for unsaved and modified file. */
	private static final ImageIcon redDisk = IconLoader.getImageIcon("icons/redDisk.png");
	
	/** String value for Croatian language. */
	private static final String HR = "Hrvatski";
	
	/** String value for English language. */
	private static final String EN = "English";
	
	/** String value for German language. */
	private static final String DE = "Deutsche";
	
	/** Panel that contains all editors. */
	private JTabbedPane tabbedPane;
	
	/** Set of unsaved indexes of files. */
	private Set<Integer> unsaved;
	
	/** List of file text editors. */
	private List<JTextArea> editors;
	
	/** Current string value of copied text. */
	private String storage;
	
	/** Label that displays number of lines in text. */
	private JLabel lengthLabel;
	
	/** Label that displays line number of caret. */
	private JLabel lnLabel;
	
	/** Label that displays number of columns in current line of text. */
	private JLabel colLabel;
	
	/** Label that displays number of selected characters in text. */
	private JLabel selLabel;
	
	/** Label that displays current date and time. */
	private Clock clock;
	
	/** Localization provider for this frame. */
	private FormLocalizationProvider flp;
	
	/** Action that opens file. */
	private Action openDocumentAction;
	
	/** Action that creates and opens new file. */
	private Action newDocumentAction;
	
	/** Action that saves file. */
	private Action saveDocumentAction;
	
	/** Action that saves file in location of users choosing. */
	private Action saveAsDocumentAction;
	
	/** Action that closes file. */
	private Action closeDocumentAction;
	
	/** Action copies and deletes selected part of text. */
	private Action cutAction;
	
	/** Action copies selected part of text. */
	private Action copyAction;
	
	/** Action inserts copied part of text into caret location. */
	private Action pasteAction;
	
	/** Action that displays statistical information of currently selected file. */
	private Action statisticsAction;
	
	/** Action that exits JNotepad++. */
	private Action exitAction;
	
	/** Action sets selected part of text to upper-case. */
	private Action upperCaseAction;
	
	/** Action sets selected part of text to lower-case. */
	private Action lowerCaseAction;
	
	/** Action inverts casing of selected part of text. */
	private Action invertCaseAction;
	
	/** 
	 * Action which sorts selected lines of text ascending, sorting is done by 
	 * standards of currently selected language. 
	 */
	private Action ascendingAction;
	
	/** 
	 * Action which sorts selected lines of text descending, sorting is done by 
	 * standards of currently selected language. 
	 */
	private Action descendingAction;
	
	/** Action which removes all duplicate lines from selected text. */
	private Action uniqueAction;

	/**
	 * Constructs new JNotepad++.
	 */
	public JNotepadPP() {
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		setSize(700, 700);
		setLocation(0, 0);
		setTitle("JNotepad++");
		
		flp = new FormLocalizationProvider(LocalizationProvider.getInstance(), this);
		
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				exit();
			}
		});
		
		intiGUI();
	}
	
	/**
	 * Exits from JNotepad++ application.
	 */
	private void exit() {
		boolean exit = true;
		for (Integer index : unsaved.toArray(new Integer[unsaved.size()])) {
			String fileName = tabbedPane.getComponentAt(index).getName();
			int result = JOptionPane.showConfirmDialog(
					JNotepadPP.this,
					flp.getString(LocalizationConstants.EXIT_MSG_KEY) + fileName,
					flp.getString(LocalizationConstants.SYS_MSG_KEY), 
					JOptionPane.YES_NO_CANCEL_OPTION
			);
			
			if (result == JOptionPane.YES_OPTION) {
				int saved = saveFile(index);
				if (saved != JOptionPane.YES_OPTION && saved != JOptionPane.NO_OPTION) {
					exit = false;
					break;
				}
			} else if (result != JOptionPane.NO_OPTION) {
				exit = false;
				break;
			}
		}
		
		if (exit) {
			clock.stop();
			dispose();
		} else {
			checkSaved();
		}
	}
	
	/**
	 * Checks if saveAction should be enabled.
	 */
	private void checkSaved() {
		int index = tabbedPane.getSelectedIndex();
		if (index < 0) {
			saveDocumentAction.setEnabled(false);
			return;
		}
		if (unsaved.contains(index)) {
			saveDocumentAction.setEnabled(true);
		} else {
			saveDocumentAction.setEnabled(false);
		}
		
	}

	/**
	 * Saves file at given index. If file does not exists method will ask user
	 * if he still wishes to continue, and will return result of operation using
	 * JOptionPane static variables as results.
	 * @param index index of file in JNotepad++
	 * @return JOptionPane static variables that represents file saving outcome
	 */
	private int saveFile(int index) {
		String newFileText = editors.get(index).getText();
		String fileName = tabbedPane.getComponentAt(index).getName();
		File file = new File(fileName);
		
		if (!file.exists()) {
			int result = JOptionPane.showConfirmDialog(
					JNotepadPP.this,
					flp.getString(LocalizationConstants.FILE_KEY) + ": " + fileName 
					+ " " + flp.getString(LocalizationConstants.SAVE_FILE_KEY),
					flp.getString(LocalizationConstants.SYS_MSG_KEY), 
					JOptionPane.YES_NO_CANCEL_OPTION
			);
			
			if (result != JOptionPane.YES_OPTION) {
				return result;
			}
		}
		
		StreamUtilities.writeOnFile(file, newFileText);
		setSaved(index);
		return JOptionPane.YES_OPTION;
	}
	
	/**
	 * Sets index of file to unsaved.
	 * @param index index of file in JNotepad++
	 */
	private void setSaved(Integer index) {
		unsaved.remove(index);
		tabbedPane.setIconAt(index, greenDisk);
		saveDocumentAction.setEnabled(false);
	}

	/**
	 * Initializes GUI.
	 */
	private void intiGUI() {
		getContentPane().setLayout(new BorderLayout());
		unsaved = new TreeSet<>();
		editors = new ArrayList<>();
		
		initCenter();
		
		createActions();
		createMenues();
		createToolbars();
	}

	/**
	 * Initializes center of JNotepad++.
	 */
	private void initCenter() {
		tabbedPane = new JTabbedPane();
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());
		getContentPane().add(panel, BorderLayout.CENTER);
		panel.add(tabbedPane, BorderLayout.CENTER);
		
		tabbedPane.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				int index = tabbedPane.getSelectedIndex();
				if (index >= 0) {
					String name = tabbedPane.getComponentAt(index).getName();
					setTitle(name + " - JNotepad++");
				} else {
					setTitle("JNotepad++");
				}
				updateStatLabels();
				checkSaved();
			}
		});
		
		JPanel labelPanel = new JPanel(new BorderLayout());
		lengthLabel = new LJLabel(LocalizationConstants.LENGTH_KEY, ""+0, SwingConstants.LEFT, flp);
		lnLabel = new LJLabel(LocalizationConstants.LN_KEY, ""+0, SwingConstants.CENTER, flp);
		colLabel = new LJLabel(LocalizationConstants.COL_KEY, ""+0, SwingConstants.CENTER, flp);
		selLabel = new LJLabel(LocalizationConstants.SEL_KEY, ""+0, SwingConstants.CENTER, flp);
		clock = new Clock(SwingConstants.RIGHT);
		clock.start();
		
		labelPanel.add(lengthLabel, BorderLayout.WEST);
		
		JPanel innerLabelPanel = new JPanel();
		innerLabelPanel.add(lnLabel);
		innerLabelPanel.add(colLabel);
		innerLabelPanel.add(selLabel);
		labelPanel.add(innerLabelPanel, BorderLayout.CENTER);
		labelPanel.add(clock, BorderLayout.EAST);
		
		panel.add(labelPanel, BorderLayout.SOUTH);
	}
	
	/**
	 * Creates tool-bars.
	 */
	private void createToolbars() {
		JToolBar toolBar = new JToolBar("Tools");
		toolBar.setFloatable(true);
		
		toolBar.add(newDocumentAction);
		toolBar.add(openDocumentAction);
		toolBar.add(saveDocumentAction);
		toolBar.add(saveAsDocumentAction);
		toolBar.add(closeDocumentAction);
		toolBar.add(exitAction);
		
		toolBar.addSeparator();
		
		toolBar.add(copyAction);
		toolBar.add(cutAction);
		toolBar.add(pasteAction);
		
		toolBar.addSeparator();
		
		toolBar.add(statisticsAction);
		
		this.getContentPane().add(toolBar, BorderLayout.PAGE_START);
	}

	/**
	 * Creates menus.
	 */
	private void createMenues() {
		JMenuBar menuBar = new JMenuBar();
		
		JMenu fileMenu = new LJMenu(LocalizationConstants.FILE_KEY, flp);
		menuBar.add(fileMenu);
		
		fileMenu.add(new JMenuItem(newDocumentAction));
		fileMenu.add(new JMenuItem(openDocumentAction));
		fileMenu.addSeparator();
		fileMenu.add(new JMenuItem(saveDocumentAction));
		fileMenu.add(new JMenuItem(saveAsDocumentAction));
		fileMenu.addSeparator();
		fileMenu.add(new JMenuItem(closeDocumentAction));
		fileMenu.add(new JMenuItem(exitAction));
		
		JMenu editMenu = new LJMenu(LocalizationConstants.EDIT_KEY, flp);
		menuBar.add(editMenu);
		
		editMenu.add(new JMenuItem(copyAction));
		editMenu.add(new JMenuItem(cutAction));
		editMenu.addSeparator();
		editMenu.add(new JMenuItem(pasteAction));
		
		JMenu statsMenu = new LJMenu(LocalizationConstants.STATS_KEY, flp);
		menuBar.add(statsMenu);
		
		statsMenu.add(new JMenuItem(statisticsAction));
		
		JMenu langMenu = new LJMenu(LocalizationConstants.LANG_KEY, flp);
		menuBar.add(langMenu);
		
		JMenuItem hr = new JMenuItem(HR);
		JMenuItem en = new JMenuItem(EN);
		JMenuItem de = new JMenuItem(DE);
		langMenu.add(hr);
		langMenu.add(en);
		langMenu.add(de);
		hr.addActionListener(new LanguageActionListener(LocalizationConstants.HR));
		en.addActionListener(new LanguageActionListener(LocalizationConstants.EN));
		de.addActionListener(new LanguageActionListener(LocalizationConstants.DE));
		
		JMenu toolsMenu = new LJMenu(LocalizationConstants.TOOLS_KEY, flp);
		JMenu caseMenu = new LJMenu(LocalizationConstants.CASE_KEY, flp);
		JMenu sortMenu = new LJMenu(LocalizationConstants.SORT_KEY, flp);
		menuBar.add(toolsMenu);
		toolsMenu.add(caseMenu);
		toolsMenu.add(sortMenu);
		
		caseMenu.add(new JMenuItem(upperCaseAction));
		caseMenu.add(new JMenuItem(lowerCaseAction));
		caseMenu.add(new JMenuItem(invertCaseAction));
		sortMenu.add(new JMenuItem(ascendingAction));
		sortMenu.add(new JMenuItem(descendingAction));
		toolsMenu.add(new JMenuItem(uniqueAction));
		
		this.setJMenuBar(menuBar);
	}
	
	/**
	 * Action listener that changes language when activated.
	 * 
	 * @author Josip Kasap
	 *
	 */
	private class LanguageActionListener implements ActionListener {
		
		/** Language of this action listener. */
		private String language;
		
		/**
		 * Constructs new language action listener with given language.
		 * @param language language of this listener
		 */
		private LanguageActionListener(String language) {
			this.language = language;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			LocalizationProvider.getInstance().setLanguage(language);
			updateStatLabels();
		}
	}

	/**
	 * Creates and initializes actions.
	 */
	private void createActions() {
		openDocumentAction = new LocalizableAction(LocalizationConstants.OPEN_KEY,
				LocalizationConstants.OPEN_DESC_KEY, flp) {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser fc = new JFileChooser();
				fc.setDialogTitle(flp.getString(LocalizationConstants.OPEN_FILE_KEY));
				if (fc.showOpenDialog(JNotepadPP.this) != JFileChooser.APPROVE_OPTION) {
					return;
				}
				File fileName = fc.getSelectedFile();
				Path filePath = fileName.toPath();
				if (!Files.isReadable(filePath)) {
					JOptionPane.showMessageDialog(
							JNotepadPP.this,
							flp.getString(LocalizationConstants.FILE_KEY) + " " + fileName.getAbsolutePath() 
							+ " " + flp.getString(LocalizationConstants.NOT_EXIST_KEY),
							flp.getString(LocalizationConstants.ERROR_KEY),
							JOptionPane.ERROR_MESSAGE
					);
					return;
				}
				
				byte[] buffer;
				try {
					buffer = Files.readAllBytes(filePath);
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(
							JNotepadPP.this,
							flp.getString(LocalizationConstants.FILE_ERROR_KEY) 
							+ fileName.getAbsolutePath() + "!",
							flp.getString(LocalizationConstants.ERROR_KEY),
							JOptionPane.ERROR_MESSAGE
					);
					return;
				}
				
				String text = new String(buffer, StandardCharsets.UTF_8);
				initOpenedFile(text, fileName);
			}
		};
		openDocumentAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control 0"));
		openDocumentAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_O);
		
		newDocumentAction = new LocalizableAction(LocalizationConstants.NEW_KEY,
				LocalizationConstants.NEW_DESC_KEY, flp) {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser fc = new JFileChooser();
				fc.setDialogTitle(flp.getString(LocalizationConstants.NEW_FILE_KEY));
				fc.setFileSelectionMode(JFileChooser.FILES_ONLY );
				if (fc.showOpenDialog(JNotepadPP.this) != JFileChooser.APPROVE_OPTION) {
					return;
				}
				
				File fileName = fc.getSelectedFile();
				if (fileName.exists()) {
					JOptionPane.showMessageDialog(
							JNotepadPP.this,
							flp.getString(LocalizationConstants.FILE_KEY) + " " + fileName.getAbsolutePath() 
							+ " " + flp.getString(LocalizationConstants.ALREADY_EXISTS_KEY),
							flp.getString(LocalizationConstants.ERROR_KEY),
							JOptionPane.ERROR_MESSAGE
					);
					return;
				}
				
				try {
					fileName.createNewFile();
				} catch (IOException e) {
					JOptionPane.showMessageDialog(
							JNotepadPP.this,
							flp.getString(LocalizationConstants.FILE_KEY) + " " + fileName.getAbsolutePath() 
							+ " " + flp.getString(LocalizationConstants.CANT_CREATE_KEY),
							flp.getString(LocalizationConstants.ERROR_KEY),
							JOptionPane.ERROR_MESSAGE
					);
					return;
				}
				
				fileName.setReadable(true);
				fileName.setWritable(true);
				initOpenedFile("", fileName);
			}
		};
		newDocumentAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control N"));
		newDocumentAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_N);
		
		saveDocumentAction = new LocalizableAction(LocalizationConstants.SAVE_KEY,
				LocalizationConstants.SAVE_DESC_KEY, flp) {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int index = tabbedPane.getSelectedIndex();
				if (!unsaved.contains(index)) return;
				saveFile(index);
			}
		};
		saveDocumentAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control S"));
		saveDocumentAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_S);
		saveDocumentAction.setEnabled(false);
		
		saveAsDocumentAction = new LocalizableAction(LocalizationConstants.SAVE_AS_KEY,
				LocalizationConstants.SAVE_AS_DESC_KEY, flp) {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int index = tabbedPane.getSelectedIndex();
				if (index < 0) return;
				
				JFileChooser fc = new JFileChooser();
				fc.setDialogTitle(flp.getString(LocalizationConstants.SAVE_AS_KEY));
				fc.setFileSelectionMode(JFileChooser.FILES_ONLY );
				if (fc.showSaveDialog(JNotepadPP.this) != JFileChooser.APPROVE_OPTION) {
					return;
				}
				
				File fileName = fc.getSelectedFile();
				if (fileName.exists()) {
					int result = JOptionPane.showConfirmDialog(
							JNotepadPP.this,
							flp.getString(LocalizationConstants.FILE_KEY) + " " + fileName.getAbsolutePath() 
							+ " " + flp.getString(LocalizationConstants.OVERWRITE_KEY),
							flp.getString(LocalizationConstants.WARNING_KEY),
							JOptionPane.WARNING_MESSAGE
					);
					
					if (result != JOptionPane.YES_OPTION) {
						return;
					}
				}
				
				tabbedPane.getComponentAt(index).setName(fileName.getAbsolutePath());
				String newFileText = editors.get(index).getText();
				StreamUtilities.writeOnFile(fileName, newFileText);
				setSaved(index);
			}
		};
		saveAsDocumentAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control alt S"));
		saveAsDocumentAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_A);
		saveAsDocumentAction.setEnabled(false);
		
		closeDocumentAction = new LocalizableAction(LocalizationConstants.CLOSE_KEY,
				LocalizationConstants.CLOSE_DESC_KEY, flp) {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int index = tabbedPane.getSelectedIndex();
				if (index < 0) return;
				
				if (!unsaved.contains(index)) {
					removeDocument(index);
					return;
				}
				
				String fileName = tabbedPane.getComponentAt(index).getName();
				int result = JOptionPane.showConfirmDialog(
						JNotepadPP.this,
						flp.getString(LocalizationConstants.EXIT_MSG_KEY) + fileName,
						flp.getString(LocalizationConstants.SYS_MSG_KEY), 
						JOptionPane.YES_NO_CANCEL_OPTION
				);
				
				if (result == JOptionPane.YES_OPTION) {
					int result2 = saveFile(index);
					if (result2 == JOptionPane.YES_OPTION || result2 == JOptionPane.NO_OPTION) {
						tabbedPane.remove(index);
						editors.remove(index);
						if (result2 == JOptionPane.NO_OPTION) {
							unsaved.remove(index);
						}
					}
				} else if (result == JOptionPane.NO_OPTION) {
					tabbedPane.remove(index);
					editors.remove(index);
					unsaved.remove(index);
				}
			}
		};
		closeDocumentAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control W"));
		closeDocumentAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_C);
		closeDocumentAction.setEnabled(false);
		
		copyAction = new LocalizableAction(LocalizationConstants.COPY_KEY,
				LocalizationConstants.COPY_DESC_KEY, flp) {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				copyText(false);
			}
		};
		copyAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control C"));
		copyAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_C);
		copyAction.setEnabled(false);
		
		cutAction = new LocalizableAction(LocalizationConstants.CUT_KEY,
				LocalizationConstants.CUT_DESC_KEY, flp) {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				copyText(true);
			}
		};
		cutAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control X"));
		cutAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_T);
		cutAction.setEnabled(false);
		
		pasteAction = new LocalizableAction(LocalizationConstants.PASTE_KEY,
				LocalizationConstants.PASTE_DESC_KEY, flp) {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (storage == null || storage.isEmpty()) return;
				
				int index = tabbedPane.getSelectedIndex();
				if (index < 0) return;
				
				JTextArea editor = editors.get(index);
				
				int len = Math.abs(editor.getCaret().getDot() - editor.getCaret().getMark());
				int offset = Math.min(editor.getCaret().getDot(), editor.getCaret().getMark());
				
				String text = editor.getText();
				String newText = text.substring(0, offset) + storage + text.substring(offset + len, text.length());
				editor.setText(newText);
			}
		};
		pasteAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control V"));
		pasteAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_P);
		pasteAction.setEnabled(false);
		
		statisticsAction = new LocalizableAction(LocalizationConstants.INFO_KEY,
				LocalizationConstants.INFO_DESC_KEY, flp) {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int index = tabbedPane.getSelectedIndex();
				if (index < 0) return;
				
				JTextArea editor = editors.get(index);
				String text = editor.getText();
				
				int charNum = text.length();
				int lines = 1;
				int charNumNonBlank = charNum;
				
				for (char c : text.toCharArray()) {
					if (Character.isWhitespace(c)) {
						charNumNonBlank--;
						if (c == '\n') {
							lines++;
						}
					}
				}
				
				JOptionPane.showMessageDialog(
						JNotepadPP.this, 
						flp.getString(LocalizationConstants.CHARS_KEY) + charNum 
						+ flp.getString(LocalizationConstants.NON_BLANK_KEY) + charNumNonBlank 
						+ flp.getString(LocalizationConstants.LINES_KEY) + lines, 
						flp.getString(LocalizationConstants.STATS_INFO_KEY), 
						JOptionPane.INFORMATION_MESSAGE
				);
			}
		};
		statisticsAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control I"));
		statisticsAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_I);
		statisticsAction.setEnabled(false);
		
		exitAction = new LocalizableAction(LocalizationConstants.EXIT_KEY,
				LocalizationConstants.EXIT_DESC_KEY, flp) {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				exit();
			}
		};
		exitAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control X"));
		exitAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_X);
		
		upperCaseAction = new LocalizableAction(LocalizationConstants.UPPERCASE_KEY,
				LocalizationConstants.UPPERCASE_DESC_KEY, flp) {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				textFunction((text) -> text.toUpperCase());
			}
		};
		upperCaseAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control U"));
		upperCaseAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_U);
		upperCaseAction.setEnabled(false);
		
		lowerCaseAction = new LocalizableAction(LocalizationConstants.LOWERCASE_KEY,
				LocalizationConstants.LOWERCASE_DESC_KEY, flp) {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				textFunction((text) -> text.toLowerCase());
			}
		};
		lowerCaseAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control L"));
		lowerCaseAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_L);
		lowerCaseAction.setEnabled(false);
		
		invertCaseAction = new LocalizableAction(LocalizationConstants.INVERTCASE_KEY,
				LocalizationConstants.INVERTCASE_DESC_KEY, flp) {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				textFunction((text) -> {
					char[] chars = text.toCharArray();
					for (int i=0; i<chars.length; i++) {
						if (Character.isUpperCase(chars[i])) {
							chars[i] = Character.toLowerCase(chars[i]);
						} else if (Character.isLowerCase(chars[i])) {
							chars[i] = Character.toUpperCase(chars[i]);
						}
					}
					return new String(chars);
				});
			}
		};
		invertCaseAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control I"));
		invertCaseAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_I);
		invertCaseAction.setEnabled(false);
		
		ascendingAction = new LocalizableAction(LocalizationConstants.ASCENDING_KEY,
				LocalizationConstants.ASCENDING_DESC_KEY, flp) {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				sortText(new Sort(true));
			}
		};
		ascendingAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control A"));
		ascendingAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_A);
		ascendingAction.setEnabled(false);
		
		descendingAction = new LocalizableAction(LocalizationConstants.DESCENDING_KEY,
				LocalizationConstants.DESCENDING_DESC_KEY, flp) {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				sortText(new Sort(false));
			}
		};
		descendingAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control D"));
		descendingAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_D);
		descendingAction.setEnabled(false);
		
		uniqueAction = new LocalizableAction(LocalizationConstants.UNIQUE_KEY,
				LocalizationConstants.UNIQUE_DESC_KEY, flp) {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				sortText((t) -> {
					if (!t.contains("\n")) return t;
					t = t.replace("\r", "");
					String[] splited = t.split("\n");
					Set<String> uniqueSet = new LinkedHashSet<>();
					Set<String> set = new HashSet<>();
					for (String line : splited) {
						uniqueSet.add(line);
						set.add(line);
					}
					StringBuilder sb = new StringBuilder();
					String NEW_LINE = String.format("%n");
					for (String line : uniqueSet) {
						sb.append(line);
						sb.append(NEW_LINE);
					}
					return sb.toString();
				});
			}
		};
		uniqueAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control Q"));
		uniqueAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_Q);
		uniqueAction.setEnabled(false);
	}
	
	/**
	 * Removes file from JNotepad++ at given index.
	 * @param index index of file which is to be removed
	 */
	private void removeDocument(int index) {
		tabbedPane.remove(index);
		editors.remove(index);
		int newIndex = tabbedPane.getSelectedIndex();
		if (newIndex < 0) {
			closeDocumentAction.setEnabled(false);
			saveAsDocumentAction.setEnabled(false);
			statisticsAction.setEnabled(false);
			saveDocumentAction.setEnabled(false);
		} else {
			if (unsaved.contains(index)) {
				saveDocumentAction.setEnabled(true);
			} else {
				saveDocumentAction.setEnabled(false);
			}
		}
	}

	/**
	 * Function that sorts lines of given string. This function
	 * sorts eater ascending or descending, depending of property
	 * ascending given at constructor. Sorting is done using rools
	 * of currently selected language.
	 * 
	 * @author Josip Kasap
	 *
	 */
	private static class Sort implements Function<String, String> {
		
		/** Flag that determines if sorting should be done ascending or descending. */
		private boolean ascending;
		
		/** String value of new line. */
		private static final String NEW_LINE = String.format("%n");
		
		/**
		 * Constructs new Sort function with given ascending flag
		 * @param ascending flag that determines if sorting should be done
		 * 			 ascending or descending
		 */
		private Sort(boolean ascending) {
			this.ascending = ascending;
		}
		
		@Override
		public String apply(String t) {
			if (!t.contains("\n")) return t;
			t = t.replace("\r", "");
			Locale locale = new Locale(LocalizationProvider.getInstance().getLanguage());
			Collator collator = Collator.getInstance(locale);
			String[] splited = t.split("\n");
			List<String> list = new ArrayList<>();
			for (String line : splited) {
				list.add(line);
			}
			if (ascending) {
				list.sort((s1, s2) -> collator.compare(s1, s2));
			} else {
				list.sort((s1, s2) -> - collator.compare(s1, s2));
			}
			StringBuilder sb = new StringBuilder();
			for (String line : list) {
				sb.append(line);
				sb.append(NEW_LINE);
			}
			return sb.toString();
		}
	}
	
	/**
	 * Sorts selected lines of editor with given {@link Function} which
	 * can sort line of String representation of that selected lines.
	 * @param sortFunction function which can sort lines in given String
	 */
	private void sortText(Function<String, String> sortFunction) {
		int index = tabbedPane.getSelectedIndex();
		if (index < 0) return;
		
		JTextArea editor = editors.get(index);
		Document doc = editor.getDocument();
		
		int len = Math.abs(editor.getCaret().getDot() - editor.getCaret().getMark());
		if (len == 0) return;
		int offset1 = Math.min(editor.getCaret().getDot(), editor.getCaret().getMark());
		
		try {
			int firstLine = editor.getLineOfOffset(offset1);
			int lastLine = editor.getLineOfOffset(offset1 + len);
			int startOffset = editor.getLineStartOffset(firstLine);
			int endOffset = editor.getLineEndOffset(lastLine);
			String text = doc.getText(startOffset, endOffset - startOffset);
			String newText = sortFunction.apply(text);
			doc.remove(startOffset, endOffset - startOffset);
			doc.insertString(startOffset, newText, null);
		} catch (BadLocationException e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}
	
	/**
	 * Function which isolates selected text from selected text editor and
	 * lets given {@link Function} to change it in what ever way it wants,
	 * then inserts changed text into old position.
	 * @param selectedFunction {@link Function} which alters some text
	 */
	private void textFunction(Function<String, String> selectedFunction) {
		int index = tabbedPane.getSelectedIndex();
		if (index < 0) return;
		
		JTextArea editor = editors.get(index);
		Document doc = editor.getDocument();
		
		int len = Math.abs(editor.getCaret().getDot() - editor.getCaret().getMark());
		if (len == 0) return;
		int offset = Math.min(editor.getCaret().getDot(), editor.getCaret().getMark());
		String selectedText = editor.getSelectedText();
		selectedText = selectedFunction.apply(selectedText);
		
		try {
			doc.remove(offset, len);
			doc.insertString(offset, selectedText, null);
		} catch (BadLocationException e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}
	
	/**
	 * Method that copies selected text, and removes it if
	 * argument remove is set to <code>true</code>.
	 * @param remove flag that determines should selected text be removed after copying
	 */
	private void copyText(boolean remove) {
		int index = tabbedPane.getSelectedIndex();
		if (index < 0) return;
		
		JTextArea editor = editors.get(index);
		Document doc = editor.getDocument();
		
		int len = Math.abs(editor.getCaret().getDot() - editor.getCaret().getMark());
		if (len == 0) return;
		int offset = Math.min(editor.getCaret().getDot(), editor.getCaret().getMark());
		
		try {
			storage = editor.getText().substring(offset, offset + len);
			
			if (remove) {
				doc.remove(offset, len);
			}
		} catch (BadLocationException e1) {
			e1.printStackTrace();
			System.exit(-1);
		}
		
		pasteAction.setEnabled(true);
	}
	
	/**
	 * Initializes newly created document.
	 * @param doc document
	 */
	private void initDocument(Document doc) {
		doc.addDocumentListener(new DocumentListener() {

			@Override
			public void changedUpdate(DocumentEvent e) {
				setUnsaved();
				updateStatLabels();
			}

			@Override
			public void insertUpdate(DocumentEvent e) {
				setUnsaved();
				updateStatLabels();
			}

			@Override
			public void removeUpdate(DocumentEvent e) {
				setUnsaved();
				updateStatLabels();
			}
			
			private void setUnsaved() {
				int index = tabbedPane.getSelectedIndex();
				tabbedPane.setIconAt(index, redDisk);
				unsaved.add(index);
				saveDocumentAction.setEnabled(true);
			}
			
		});
	}
	
	/**
	 * Updates status labels at the bottom of JNotepad++.
	 */
	private void updateStatLabels() {
		int index = tabbedPane.getSelectedIndex();
		if (index < 0) {
			setLabels(0,0,0,0);
			return;
		}
		
		JTextArea editor = editors.get(index);
		String text = editor.getText();
		
		int length = text.length();
		if (length == 0) {
			setLabels(0,0,0,0);
			return;
		}
		int position = editor.getCaret().getDot();
		position = position >= length ? length - 1 : position;
		int col = 0;
		int sel = Math.abs(editor.getCaret().getDot() - editor.getCaret().getMark());
		int ln = 0;
		try {
			ln = editor.getLineOfOffset(position) + 1;
		} catch (BadLocationException e) {
			e.printStackTrace();
			System.exit(-1);
		}
		char[] chars = text.toCharArray();
		
		while (position >= 0) {
			if (chars[position] == '\n' || chars[position] == '\r') {
				break;
			}
			position--;
			col++;
		}
		
		setLabels(length, ln, col, sel);
	}
	
	/**
	 * Sets status labels to given values.
	 * @param length length of whole text
	 * @param ln line number of caret
	 * @param col column in line of caret
	 * @param sel number of selected chars
	 */
	private void setLabels(int length, int ln, int col, int sel) {
		lengthLabel.setText("" + length);
		lengthLabel.updateUI();
		lnLabel.setText("" + ln);
		lnLabel.updateUI();
		colLabel.setText("" + col);
		colLabel.updateUI();
		selLabel.setText("" + sel);
		selLabel.updateUI();
	}
	
	/**
	 * Method that does all necessary operations upon opening file.
	 * @param text text of file
	 * @param fileName file name
	 */
	private void initOpenedFile(String text, File fileName) {
		JTextArea editor = new JTextArea();
		editor.setText(text);
		editors.add(editor);
		
		initDocument(editor.getDocument());
		editor.addCaretListener(new CaretListener() {
			@Override
			public void caretUpdate(CaretEvent evt) {
				updateStatLabels();
				String text = editor.getSelectedText();
				if (text == null || text.isEmpty()) {
					setTextActions(false);
				} else {
					setTextActions(true);
				}
			}

			private void setTextActions(boolean enable) {
				upperCaseAction.setEnabled(enable);
				lowerCaseAction.setEnabled(enable);
				invertCaseAction.setEnabled(enable);
				ascendingAction.setEnabled(enable);
				descendingAction.setEnabled(enable);
				uniqueAction.setEnabled(enable);
				copyAction.setEnabled(enable);
				cutAction.setEnabled(enable);
			}
		});
		
		JScrollPane comp = new JScrollPane(editor);
		comp.setName(fileName.getAbsolutePath());
		tabbedPane.addTab(fileName.getName(), comp);
		
		tabbedPane.setSelectedComponent(comp);
		int index = tabbedPane.getSelectedIndex();
		tabbedPane.setIconAt(index, greenDisk);
		
		tabbedPane.setToolTipTextAt(index, fileName.getAbsolutePath());
		
		saveAsDocumentAction.setEnabled(true);
		closeDocumentAction.setEnabled(true);
		statisticsAction.setEnabled(true);
	}
	
	/**
	 * Main method which starts JNotepad++ program.
	 * @param args command-line arguments (unused)
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> new JNotepadPP().setVisible(true));
	}
}
