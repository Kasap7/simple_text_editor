package hr.fer.zemris.java.hw11.jnotepadpp;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JLabel;
import javax.swing.SwingUtilities;

/**
 * Clock is implementation of {@link JLabel} which periodically changes its text
 * into current date and time. Date and time are written in format: yyyy/MM/dd HH:mm:ss.
 * This clock can stop producing time when is requested, when he is stopped, his text
 * will remain as current time and date value before he is stopped.
 * 
 * @author Josip Kasap
 *
 */
public class Clock extends JLabel {
	
	/** Default serial version. */
	private static final long serialVersionUID = 1L;
	
	/** Format of date and time. */
	public static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	
	/** Flag that determines if clock should stop. */
	private volatile boolean stopRequested;
	
	private Thread timeThread;
	
	/**
	 * Constructs new Clock with given horizontal alignment.
	 * @param horizontalAlignment horizontal alignment
	 */
	public Clock(int horizontalAlignment) {
		updateTime();
		setHorizontalAlignment(horizontalAlignment);
		
		timeThread = new Thread(() -> {
			while (true) {
				try {
					Thread.sleep(500);
					} catch (Exception ignorable) {}
				if (stopRequested) break;
				SwingUtilities.invokeLater(()-> updateTime());
			}
		});
		
		updateTime();
		timeThread.setDaemon(true);
	}

	/**
	 * Updates time of clock.
	 */
	private void updateTime() {
		setText(DATE_FORMAT.format(new Date()));
		updateUI();
	}
	
	/**
	 * Starts producing and rendering time.
	 */
	public void start() {
		stopRequested = false;
		if (!timeThread.isAlive()) {
			timeThread.start();
		}
	}
	
	/**
	 * Stops producing new time. Label value will remain at old time.
	 */
	public void stop() {
		stopRequested = true;
	}
}
