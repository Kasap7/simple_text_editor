package hr.fer.zemris.java.hw11.jnotepadpp;

import java.io.IOException;
import java.io.InputStream;

import javax.swing.ImageIcon;

import hr.fer.zemris.java.hw11.jnotepadpp.utils.StreamUtilities;

/**
 * IconLoader is utility class used to load any icon with given name
 * from standard position of icons of this project.
 * 
 * @author Josip Kasap
 *
 */
public class IconLoader {
	
	/**
	 * Private constructor to unable creating of this class.
	 */
	private IconLoader() {
		super();
	}

	/**
	 * Returns {@link ImageIcon} from String representation of path
	 * to image that represents that icon
	 * @param path String value of path to image of icon
	 * @return {@link ImageIcon} from String representation of path
	 * @throws IllegalArgumentException if path is null value
	 */
	public static ImageIcon getImageIcon(String path) {
		if (path == null) {
			throw new IllegalArgumentException("Path cannot be null value.");
		}
		InputStream is = IconLoader.class.getResourceAsStream(path);
		if(is==null) {
			System.err.println("Unexpected error happened. Terminating program");
			System.exit(-1);
		}
		
		byte[] bytesR = StreamUtilities.readAllBytes(is);
		try {
			if (is != null) {
				is.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(-1);
		}
		
		return new ImageIcon(bytesR);
	}
}
